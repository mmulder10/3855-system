import connexion
from connexion import NoContent

from pykafka import KafkaClient
from pykafka.common import OffsetType
import json

import logging.config
import yaml

# External Application Configuration
with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())

# External Logging Configuration
with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')


def get_blood_pressure_reading(index):
    """ Get BP Reading in History """
    hostname = "%s:%d" % (app_config["events"]["hostname"], app_config["events"]["port"])
    client = KafkaClient(hosts=hostname)
    topic = client.topics[app_config["events"]["topic"]]
    consumer = topic.get_simple_consumer(reset_offset_on_start=True, consumer_timeout_ms=100)

    logger.info("Retrieving BP at index %d" % index)

    count = 0
    reading = None
    for msg in consumer:
        msg_str = msg.value.decode('utf-8')
        msg = json.loads(msg_str)

        if msg["type"] == "bp":

            if count == index:
                reading = msg["payload"]
                return reading, 200

            count += 1

    logger.error("Coiuld not find BP at index %d" % index)
    return { "message": "Not Found"}, 404

def get_heart_rate_reading(index):
    """ Get HR Reading in History """
    hostname = "%s:%d" % (app_config["events"]["hostname"], app_config["events"]["port"])
    client = KafkaClient(hosts=hostname)
    topic = client.topics[app_config["events"]["topic"]]
    consumer = topic.get_simple_consumer(reset_offset_on_start=True, consumer_timeout_ms=100)

    logger.info("Retrieving HR at index %d" % index)

    count = 0
    reading = None
    for msg in consumer:
        msg_str = msg.value.decode('utf-8')
        msg = json.loads(msg_str)

        if msg["type"] == "hr":

            if count == index:
                reading = msg["payload"]
                return reading, 200

            count += 1

    logger.error("Coiuld not find HR at index %d" % index)
    return { "message": "Not Found"}, 404

app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yml", strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    app.run(port=8110)
