import connexion
from connexion import NoContent

import datetime
import os

from apscheduler.schedulers.background import BackgroundScheduler
import json
import requests


import logging.config
import yaml

# External Application Configuration
with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())

# External Logging Configuration
with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')


def get_stats():
    """ Gets processing stats """

    stats = {}
    if os.path.isfile(app_config["datastore"]["filename"]):

        stats_file = open(app_config["datastore"]["filename"])

        data = stats_file.read()

        stats_file.close()

        full_stats = json.loads(data)

        if "num_bp_readings" in full_stats:
            stats["num_bp_readings"] = full_stats["num_bp_readings"]
        if "max_bp_sys_reading" in full_stats:
            stats["max_bp_sys_reading"] = full_stats["max_bp_sys_reading"]
        if "max_bp_dia_reading" in full_stats:
            stats["max_bp_dia_reading"] = full_stats["max_bp_dia_reading"]
        if "num_hr_readings" in full_stats:
            stats["num_hr_readings"] = full_stats["num_hr_readings"]
        if "max_hr_reading" in full_stats:
            stats["max_hr_reading"] = full_stats["max_hr_reading"]

        logger.info("Found valid stats")
        logger.debug(stats)
    else:
        return "Statistics Do Not Exist", 404

    return stats, 200


def populate_stats():
    """ Periodically update stats """
    logger.info("Processing")
    stats = {}
    if os.path.isfile(app_config["datastore"]["filename"]):

        stats_file = open(app_config["datastore"]["filename"])

        data = stats_file.read()
        stats = json.loads(data)

        stats_file.close()

    last_updated = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
    if "last_updated" in stats:
        last_updated = stats["last_updated"]

    response = requests.get(app_config["eventstore"]["url"] + "/blood-pressure?timestamp=" + last_updated)

    if response.status_code == 200:
        if "num_bp_readings" in stats.keys():
            stats["num_bp_readings"] += len(response.json())
        else:
            stats["num_bp_readings"] = len(response.json())

        for event in response.json():
            if "max_bp_sys_reading" in stats.keys() and \
                event["blood_pressure"]["systolic"] > stats["max_bp_sys_reading"]:
                stats["max_bp_sys_reading"] = event["blood_pressure"]["systolic"]
            elif "max_bp_sys_reading" not in stats.keys():
                stats["max_bp_sys_reading"] = event["blood_pressure"]["systolic"]

            if "max_bp_dia_reading" in stats.keys() and \
                event["blood_pressure"]["diastolic"] > stats["max_bp_dia_reading"]:
                stats["max_bp_dia_reading"] = event["blood_pressure"]["diastolic"]
            elif "max_bp_sys_reading" not in stats.keys():
                stats["max_bp_dia_reading"] = event["blood_pressure"]["diastolic"]

    response = requests.get(app_config["eventstore"]["url"] + "/heart-rate?timestamp=" + last_updated)

    if response.status_code == 200:
        if "num_hr_readings" in stats.keys():
            stats["num_hr_readings"] += len(response.json())
        else:
            stats["num_hr_readings"] = len(response.json())

        for event in response.json():
            if "max_hr_reading" in stats.keys() and \
                    event["heart_rate"] > stats["max_hr_reading"]:
                stats["max_hr_reading"] = event["heart_rate"]
            elif "max_hr_reading" not in stats.keys():
                stats["max_hr_reading"] = event["heart_rate"]

    stats["last_updated"] = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

    stats_file = open(app_config["datastore"]["filename"], "w")

    stats_file.write(json.dumps(stats))

    stats_file.close()

    logger.info("Done Processing")




def init_scheduler():
    """ Initializes the periodic background processing """
    sched = BackgroundScheduler(daemon=True)
    sched.add_job(populate_stats,
                  'interval',
                  seconds=app_config['scheduler']['period_sec'])
    sched.start()


app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yml", strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    init_scheduler()
    app.run(port=8100)
