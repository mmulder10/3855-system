import connexion
from connexion import NoContent

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from base import Base
from blood_pressure import BloodPressure
from heart_rate import HeartRate

import datetime
from threading import Thread
from sqlalchemy import func

from pykafka import KafkaClient
from pykafka.common import OffsetType
import json

import logging
import logging.config
import yaml

# External Application Configuration
with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())

# External Logging Configuration
with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

DB_ENGINE = create_engine('mysql+pymysql://%s:%s@%s:%d/%s' % (app_config["datastore"]["user"],
                                        app_config["datastore"]["password"],
                                        app_config["datastore"]["hostname"],
                                        app_config["datastore"]["port"],
                                        app_config["datastore"]["db"]))


Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)


def report_blood_pressure_reading(body):
    """ Receives a blood pressure reading """

    session = DB_SESSION()

    bp = BloodPressure(body['patient_id'],
                       body['device_id'],
                       body['timestamp'],
                       body['blood_pressure']['systolic'],
                       body['blood_pressure']['diastolic'])

    session.add(bp)

    session.commit()
    session.close()

    logger.debug("Stored blood-pressure request with a unique id of %s" % body["device_id"])

    return NoContent, 201


def report_heart_rate_reading(body):
    """ Receives a heart rate (pulse) reading """

    session = DB_SESSION()

    hr = HeartRate(body['patient_id'],
                   body['device_id'],
                   body['timestamp'],
                   body['heart_rate'])

    session.add(hr)

    session.commit()
    session.close()

    logger.debug("Stored heart-rate request with a unique id of %s" % body["device_id"])

    return NoContent, 201


def get_blood_pressure_readings(timestamp):
    """ Gets new blood pressure readings after the timestamp """

    session = DB_SESSION()

    timestamp_datetime = datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S")
    print(timestamp_datetime)

    results = session.query(BloodPressure).filter(BloodPressure.date_created >= timestamp_datetime)

    results_list = []

    for result in results:
        results_list.append(result.to_dict())

    session.close()

    logger.info("Query for Blood Pressure readings after %s returns %d results" % (timestamp, len(results_list)))

    return results_list, 200

def get_heart_rate_readings(timestamp):
    """ Gets new heart rate readings after the timestamp """
    session = DB_SESSION()

    timestamp_datetime = datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S")
    print(timestamp_datetime)

    results = session.query(HeartRate).filter(HeartRate.date_created >= timestamp_datetime)

    results_list = []

    for result in results:
        results_list.append(result.to_dict())

    session.close()

    return results_list, 200


def process_messages():
    """ Process event messages """
    hostname = "%s:%d" % (app_config["events"]["hostname"], app_config["events"]["port"])
    client = KafkaClient(hosts=hostname)
    topic = client.topics[app_config["events"]["topic"]]

    # Create a consume on a consumer group, that only reads new messages (uncommitted messages)
    # when the service re-starts (i.e., it doesn't read all the old messages from the history
    # in the message queue).
    consumer = topic.get_simple_consumer(consumer_group='event_group',
                                         reset_offset_on_start=False,
                                         auto_offset_reset=OffsetType.LATEST)

    # This is blocking - it will wait for a new message
    for msg in consumer:
        msg_str = msg.value.decode('utf-8')
        msg = json.loads(msg_str)
        logger.info("Message: %s" % msg)

        payload = msg["payload"]

        if msg["type"] == "bp":
            store_blood_pressure_reading(payload)
        elif msg["type"] == "hr":
            store_heart_rate_reading(payload)

        # Commit the new message as being read
        consumer.commit_offsets()


def store_heart_rate_reading(body):
    """ Receives a heart rate (pulse) reading """

    session = DB_SESSION()

    hr = HeartRate(body['patient_id'],
                   body['device_id'],
                   body['timestamp'],
                   body['heart_rate'])

    session.add(hr)

    session.commit()
    session.close()

    logger.debug("Stored heart-rate request with a unique id of %s" % body["device_id"])


def store_blood_pressure_reading(body):
    """ Receives a blood pressure reading """

    session = DB_SESSION()

    bp = BloodPressure(body['patient_id'],
                       body['device_id'],
                       body['timestamp'],
                       body['blood_pressure']['systolic'],
                       body['blood_pressure']['diastolic'])

    session.add(bp)

    session.commit()
    session.close()

    logger.debug("Stored blood-pressure request with a unique id of %s" % body["device_id"])



app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yml", strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    logger.info("Connecting to DB. Hostname:%s, Port:%d" % (app_config["datastore"]["hostname"],
                                                            app_config["datastore"]["port"]))
    t1 = Thread(target=process_messages)
    t1.setDaemon(True)
    t1.start()

    app.run(port=8090)
