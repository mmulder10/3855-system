import connexion
from connexion import NoContent

import requests
import yaml
import logging
import logging.config
from pykafka import KafkaClient
import datetime
import json

# External Application Configuration
with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())

# External Logging Configuration
with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

hostname = "%s:%d" % (app_config["events"]["hostname"], app_config["events"]["port"])


def report_blood_pressure_reading(body):
    """ Receives a blood pressure reading """

    logger.info("Received event blood-pressure with a unique id of %s" % body["device_id"])
    # response = requests.post(app_config["blood-pressure"]["url"], json=body)
    client = KafkaClient(hosts=hostname)
    topic = client.topics[app_config["events"]["topic"]]
    producer = topic.get_sync_producer()
    msg = {"type": "bp",
           "datetime":
               datetime.datetime.now().strftime(
                   "%Y-%m-%dT%H:%M:%S"),
           "payload": body}
    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))

    logger.info("Returned event blood-pressure response (Id: %s)" %
                (body["device_id"]))

    return NoContent, 201


def report_heart_rate_reading(body):
    """ Receives a heart rate (pulse) reading """

    logger.info("Received event heart-rate with a unique id of %s" % body["device_id"])
    #response = requests.post(app_config["heart-rate"]["url"], json=body)
    client = KafkaClient(hosts=hostname)
    topic = client.topics[app_config["events"]["topic"]]
    producer = topic.get_sync_producer()
    msg = {"type": "hr",
           "datetime":
               datetime.datetime.now().strftime(
                   "%Y-%m-%dT%H:%M:%S"),
           "payload": body}
    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))
    logger.info("Returned event heart-rate response (Id: %s)" %
                (body["device_id"]))

    return NoContent, 201


app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yml", strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    app.run(port=8080)
